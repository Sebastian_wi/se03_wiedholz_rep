import java.util.Scanner;

public class Aufgabe5BMIRechner {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double gewicht;
		double gr��e;
		double bmi;
		char geschlecht;
		
		
		System.out.println("Bitte Gewicht in kg angeben ");
		gewicht= sc.nextDouble();
		System.out.println("Bitte Gr��e in m angeben ");
		gr��e= sc.nextDouble();
		System.out.println("Bitte Geschlecht angeben (m/w)");
		geschlecht= sc.next().charAt(0);
		
		bmi=berechneBmi(gewicht,gr��e);
		
		if(geschlecht=='w') {
			if(bmi<19) {
				System.out.println("Ihr BMI ist "+bmi+" sie haben Untergewicht");
			}
			if(bmi>=19 && bmi <=24) {
				System.out.println("Ihr BMI ist "+bmi+" sie haben Normalgewicht");
			}
			if(bmi>24) {
				System.out.println("Ihr BMI ist "+bmi+" sie haben �bergewicht");
			}
		}
		if(geschlecht=='m') {
			if(bmi<20) {
				System.out.println("Ihr BMI ist "+bmi+" sie haben Untergewicht");
			}
			if(bmi>=20 && bmi <=25) {
				System.out.println("Ihr BMI ist "+bmi+" sie haben Normalgewicht");
			}
			if(bmi>25) {
				System.out.println("Ihr BMI ist "+bmi+" sie haben �bergewicht");
			}
			
		}
		if(geschlecht!='m'&&geschlecht!='w') {
			System.out.println("Ung�ltige Eingabe");
		}
		
		
		
	}

	public static double berechneBmi(double z1,double z2) {
		double erg= z1/(z2*z2);
		return erg;
	}
}
