import java.util.Scanner;

public class Aufgabe4Rabattsystem {

	public static void main(String[] args) {
		double netto;
		double rabatt;
		double mwst = 0.19;
		double preis;
		double brutto;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte geben sie den Bestellwert ein: ");
		netto= sc.nextDouble();
		rabatt=getRabatt(netto);
		brutto= netto+(netto*mwst);
		System.out.println(brutto);
		
		preis= brutto-(brutto*rabatt);
		System.out.println("Der Preis betr�gt " + preis + "�");
		

	}

	
	public static double getRabatt(double netto) {
		if(netto <= 100)return 0.10;
		if(netto >100 && netto <=500)return 0.15;
		else return 0.20;
				
	}
}
