
public class MethodenAuswahlstrukturBeispiel {

	public static void main(String[] args) {
		
		int minZahl = min(12,9);
		System.out.println("die kleinere Zahl ist "+ minZahl);
		int maxZahl = max(801, 65);
		System.out.println("die gr��ere Zahl ist "+ maxZahl);
		
		int erg2= doit(3,7,'+');
		int erg3= doit(3,7,'-');
		int erg4= doit(3,7,'?');
		System.out.println(erg2);
		System.out.println(erg3);
		System.out.println(erg4);
	}
	
	public static int min(int z1, int z2){
		if(z1<z2) {
			return z1;
		}
		else return z2;
	}
	public static int max(int z1, int z2){
		if(z1>z2) {
			return z1;
		}
		else return z2;
	}
	
	
	public static int doit(int z1, int z2, char operator) {
		if(operator == '+') {
			int erg= z1+z2;
			return erg;
		}
		if(operator == '-') {
			int erg= z1-z2;
			return erg;
		}
		else return -999;
	}
}
