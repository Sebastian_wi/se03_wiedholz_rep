import java.util.Scanner;

public class Aufgabe8Schaltjahr {

	public static void main(String[] args) {
		int jahreszahl;
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte Jahreszahl eingeben: ");
		jahreszahl= sc.nextInt();
		if(jahreszahl==0) {
			System.out.println("0 ist keine Jahresahl");
		}else {
			if(jahreszahl%4==0) {
				if(jahreszahl<1582) {
					if(jahreszahl%100==0) {
						if(jahreszahl%400==0) {
							System.out.println(jahreszahl + " ist ein Schaltjahr.");
							}else {
							System.out.println(jahreszahl+ " ist kein Schaltjahr.");
							}
					}else {
					System.out.println(jahreszahl + " ist ein Schaltjahr.");
				}
			}else {
			System.out.println(jahreszahl+ " ist kein Schaltjahr.");
			}
			}
		}
	}
}
		